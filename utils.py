#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
about what
'''
import os
import time
from datetime import datetime, timedelta


# filter data which published time more then three days
def effective_date(timestamp, day):
    if timestamp:
        now = datetime.now()
        before = (now - timedelta(day)).strftime("%Y-%m-%dT%H:%M:%S")
        before_time = int(time.mktime(time.strptime(before, '%Y-%m-%dT%H:%M:%S')))
        if timestamp >= before_time:
            return True
        else:
            return False
    else:
        return False


def today_published(timestamp):
    if timestamp:
        now = datetime.now()
        before = (now - timedelta(1)).strftime("%Y-%m-%dT00:00:00")
        before_time = int(time.mktime(time.strptime(before, '%Y-%m-%dT00:00:00')))
        if timestamp >= before_time:
            return True
        else:
            return False
    else:
        return False


# get the absolute path for file
def get_absolute_path(file_name):
    worker_path = os.getcwd()
    return os.path.join(worker_path, file_name)


def get_create_date():
    return int(time.time() * 1000)


if __name__ == '__main__':
    # print(today_published(123))
    print(get_create_date())
    print('main over')
