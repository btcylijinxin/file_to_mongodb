#!/usr/bin/python3
# -*- coding: utf-8 -*-
# @Time    : 19-1-25 上午11:40
# @Author  : xiaoge
# @File    : test.py
# @Software: PyCharm

import requests
import json
from hashlib import md5


def func():
    url = 'http://api01.bitspaceman.com:8000/post/weixinpro?uid=rmrbwx&' \
          'apikey=zeJM3LvuH6pQJdNPECPrbUIBYirOfdQw4RsoGrVQQ6Dp9sJQSPeD2RZeuAwigJ4t'

    response = requests.get(url)
    json_data = json.loads(response.text)
    data = json_data['data']
    for da in data:
        uid = '%s_%s' % (da['title'], da['posterId'])
        md5_uid = make_hash(uid)
        print(md5_uid)


def make_hash(value):
    return md5(value.encode("utf8")).hexdigest()


if __name__ == '__main__':
    func()
    print('main over')
