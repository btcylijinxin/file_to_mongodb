#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
about what
'''
import os
import logging


def get_log(log_name):
    log_filename = '/root/nginx/tornado-api/huagui/nginx_tools/nginx_realtimeapi/logs_2/file_to_mongodb/log/{}'.format(log_name)

    # log_filename = './{}'.format(log_name)

    # 1、创建一个logger
    logger = logging.getLogger('mylogger')
    logger.setLevel(logging.INFO)

    # 2、创建一个handler，用于写入日志文件
    fh = logging.FileHandler(log_filename)
    # fh.setLevel(logging.INFO)

    # 3、定义handler的输出格式（formatter）
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    # 4、给handler添加formatter
    fh.setFormatter(formatter)

    # 5、给logger添加handler
    logger.addHandler(fh)
    return logger


if __name__ == '__main__':
    print('main over')
