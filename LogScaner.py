#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
scan logfile
'''

import os
import re
import sys
import time
import json
import pickle
import asyncio
from datetime import datetime

from SaveDate import SaveToDB
from SaveWeiXin import WX
from siteConfig import blacklist, siteConfig
# from getLog import get_log

from weChatSave import SaveWeChat
from xiaohongshu import XiaoHongShu


class LogScaner(object):
    def __init__(self, log_file, seek_file='./log-inc-scan.seek.temp'):
        self.log_file = log_file
        self.seek_file = seek_file
        self.save = SaveToDB()
        self.weixin = WX()
        self.wechat = SaveWeChat()
        self.xiaohongshu = XiaoHongShu()
        # self.logger = get_log('data.log')
        self.noneed = blacklist
        self.tmp = []

    async def scan(self):
        seek = self._get_seek()
        # file_mtime = os.path.getmtime(self.log_file)
        # if file_mtime <= seek['time']:
        #     print('file mtime not change since last scan')
        #     seek['time'] = file_mtime
        #     self._dump_seek(seek)
        #     return None

        file_size = os.path.getsize(self.log_file)
        if file_size < seek['position']:
            self.reset_seek()
            return None
            # sys.exit()
            # print('file size not change since last scan')
            # seek['position'] = file_size
            # self._dump_seek(seek)
            # return None

        print('file changed,start to scan')
        match_dict = dict()
        with open(self.log_file, 'rb') as logfd:
            logfd.seek(seek['position'], os.SEEK_SET)
            for line in logfd:
                if 'base64' in line.decode('utf-8', 'ignore'):
                    request_time = re.search('(\d{4}/\d{2}/\d{2} \d{2}:\d{2}:\d{2}) ', line.decode('utf-8')).group(1)
                    key = re.search('base64: (.+?) tornado body', line.decode('utf-8')).group(1)
                    value = re.search('tornado body: (.+?) while sending', line.decode('utf-8')).group(1)
                    if key:
                        if key not in match_dict:
                            match_dict[key] = dict()
                            match_dict[key]['time'] = request_time
                            match_dict[key]['value'] = value
                        else:
                            match_dict[key]['value'] += value

                else:
                    pass
            seek = {'time': time.time(), 'position': logfd.tell()}
            # print(seek)
            self._dump_seek(seek)
        await self.filter_data(match_dict)

    async def filter_data(self, data_dict):
        for _, value in data_dict.items():
            request_time = value['time']
            # print(request_time)
            content = value['value']
            try:
                json_data = json.loads(content)
            except:
                print(_)
                print(value)
                continue
                # sys.exit()
            if 'dataType' in json_data and json_data['dataType'] == 'post' or json_data['dataType'] == 'news':
                if 'data' in json_data and json_data['data']:
                    # pass
                    await self.sub_filter(json_data, request_time)
            else:
                pass
                # print(json_data['dataType'], 'data type is not allowed')

    async def sub_filter(self, json_data, request_time):
        app_code = json_data['appCode']
        if app_code not in self.noneed:
            if app_code in siteConfig:
                tmp = []
                for data in json_data['data']:
                    data['appCode'] = siteConfig[app_code]['appCode']
                    data['top_domain'] = siteConfig[app_code]['top_domain']
                    data['sourceType'] = siteConfig[app_code]['sourceType']
                    data['sourceRegion'] = siteConfig[app_code]['sourceRegion']
                    data['siteOriginId'] = int(siteConfig[app_code]['siteOriginId'])
                    data['name'] = siteConfig[app_code]['appName']
                    data['createDate'] = int(time.time() * 1000)
                    tmp.append(data)
                if 'weixin' in app_code:
                    # 微信mongodb存储
                    await self.wechat.save_data(tmp, request_time, app_code)
                    # 通用爬虫存储微信数据
                    await self.weixin.save(tmp)
                elif 'xiaohongshu' in app_code:
                    await self.xiaohongshu.save(tmp)
                    # await self.save.save(tmp)
                else:
                    # 存储其他数据
                    await self.save.save(tmp)
            else:
                print(app_code, 'not has not config')
                # if app_code not in self.tmp:
                #     self.tmp.append(app_code)
                    # self.logger.warning(app_code)

    def _get_seek(self):
        seek = {'time': time.time(), 'position': 0}
        if os.path.exists(self.seek_file):
            with open(self.seek_file, 'rb') as seekfd:
                try:
                    seek = pickle.load(seekfd)
                except:
                    pass
        else:
            self.reset_seek()
        # print(seek)
        return seek

    def _dump_seek(self, seek):
        with open(self.seek_file, 'wb') as seekfd:
            pickle.dump(seek, seekfd)

    def reset_seek(self):
        self._dump_seek({'time': time.time(), 'position': 0})


if __name__ == "__main__":
    file_path = '{}.log'.format(datetime.now().strftime('%Y%m%d'))
    if os.path.exists(file_path):
        scaner = LogScaner(file_path)
        scaner.reset_seek()
        while True:
            loop = asyncio.get_event_loop()
            loop.run_until_complete(scaner.scan())
            time.sleep(5)
