#! /usr/bin/python3
# -*- coding: utf-8 -#-
'''
start server
'''
import os
import sys
import time
import asyncio
from datetime import datetime

from LogScaner import LogScaner
from Checker import Checker
from CheckerWechat import CheckerWechat


def run():
    while True:
        flag = datetime.today().strftime('%Y-%m-%d')
        Checker()

        c = CheckerWechat()
        # c.reload()

        # log_path = 'rmrbwx.txt'
        log_path = '/root/nginx/tornado-api/huagui/nginx_tools/nginx_realtimeapi/logs_2/tornado_error.log'
        if not os.path.exists(log_path):
            print('logfile not exist')
            time.sleep(10)
            continue
        scaner = LogScaner(log_path)
        scaner.reset_seek()
        while True:
            if not os.path.exists(log_path):
                print('logfile not exist')
                time.sleep(10)
                continue
            today = datetime.today().strftime('%Y-%m-%d')
            if today != flag:
                sys.exit()
            loop = asyncio.get_event_loop()
            loop.run_until_complete(scaner.scan())
            time.sleep(5)


if __name__ == '__main__':
    run()
    print('main over')
